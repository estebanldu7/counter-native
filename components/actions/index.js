import React, {Component, Fragment} from 'react';
import {Text, TouchableOpacity} from 'react-native';

const Actions = (props) => {
    return(
        <Fragment>
            <TouchableOpacity onPress={props.reset} style={{
                width: '45%',
                height: 50,
                marginTop: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'brown',
            }}>
                <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>Reset</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.addTen} style={{
                width: '45%',
                height: 50,
                marginTop: 20,
                marginLeft: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'brown',
            }}>
                <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>+10</Text>
            </TouchableOpacity>
        </Fragment>
    )
};

export default Actions;

