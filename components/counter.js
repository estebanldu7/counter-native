import React, { useState } from 'react'

import {View, Text,} from 'react-native';

const Counter = (props) => {
    return(
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 40, color: 'white'}}>{props.number}</Text>
        </View>
    )
}

export default Counter
