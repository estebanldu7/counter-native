import React, {useState} from 'react'
import {View, Text, TouchableOpacity} from 'react-native';
import Actions from './actions';
import Counter from './counter';

const Viewer = () => {
    const [count, setCount] = useState(0);
    const clickUp = () => setCount(prevCount => prevCount + 1);
    const clickDown = () => setCount(prevCount => prevCount - 1);
    const addTen = () => setCount(prevCount => prevCount + 10);
    const resetCounter = () => setCount(0);

  return(
      <View>
          <View style={{height: 50, width: '100%', flexDirection: 'row', paddingLeft: 20, paddingRight: 20}}>
              <TouchableOpacity onPress={clickDown} style={{
                  width: 50,
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'yellow',
              }}>
                  <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>-</Text>
              </TouchableOpacity>
              <Counter number={count}/>
              <TouchableOpacity onPress={clickUp} style={{
                  width: 50,
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'yellow',
              }}>
                  <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>+</Text>
              </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Actions reset={resetCounter} addTen={addTen}/>
          </View>

      </View>
  )
};

export default Viewer;
